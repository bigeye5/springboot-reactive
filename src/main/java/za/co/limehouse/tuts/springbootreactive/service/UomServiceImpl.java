package za.co.limehouse.tuts.springbootreactive.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import za.co.limehouse.tuts.springbootreactive.domain.Uom;
import za.co.limehouse.tuts.springbootreactive.repository.UomRepository;

import java.util.Map;

@Service
public class UomServiceImpl implements UomService {

    private UomRepository uomRepository;

    @Autowired
    public UomServiceImpl(UomRepository uomRepository) {
        this.uomRepository = uomRepository;
    }

    @Override
    public Flux<Uom> findAll() {
        return uomRepository.findAll();
    }

    @Override
    public Mono<Uom> findById(String id) {
        return uomRepository.findById(id);
    }

    @Override
    public Flux<Uom> search(Map<String, String> searchFilter) {
        String searchTerm = searchFilter.get("description");
        // searchFilter could be multiple params with logic to decide what to call...
        return uomRepository.findByDescriptionContainingIgnoreCase(searchTerm);
    }

    @Override
    public Mono<Uom> insert(Uom uom) {
        return  uomRepository.insert(uom);
    }

    @Override
    public Mono<Uom> update(Uom uom) {
        return uomRepository.save(uom);
    }

    @Override
    public Mono<Void> deleteById(String id) {
        return uomRepository.deleteById(id);
    }

}
