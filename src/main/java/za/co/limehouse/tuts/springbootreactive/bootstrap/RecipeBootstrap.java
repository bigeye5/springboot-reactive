package za.co.limehouse.tuts.springbootreactive.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import za.co.limehouse.tuts.springbootreactive.domain.Uom;
import za.co.limehouse.tuts.springbootreactive.repository.UomRepository;


@Slf4j
@Component
public class RecipeBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private static final boolean LOAD_BOOTSTRAP_DATA = false;
    private final UomRepository uomRepository;

    
    public RecipeBootstrap(UomRepository uomRepository) {
        this.uomRepository = uomRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (LOAD_BOOTSTRAP_DATA) {
            loadUom();
        }
    }

    private void loadUom() {
        log.info(">>> Running bootstrap UOM data insert");
        Uom uom1 = new Uom();
        uom1.setDescription("Teaspoon");
        uomRepository.save(uom1).block();

        Uom uom2 = new Uom();
        uom2.setDescription("Tablespoon");
        uomRepository.save(uom2).block();

        Uom uom3 = new Uom();
        uom3.setDescription("Cup");
        uomRepository.save(uom3).block();

        Uom uom4 = new Uom();
        uom4.setDescription("Half Cup");
        uomRepository.save(uom4).block();

        Uom uom5 = new Uom();
        uom5.setDescription("Ounce");
        uomRepository.save(uom5).block();

        Uom uom6 = new Uom();
        uom6.setDescription("Each");
        uomRepository.save(uom6).block();

        Uom uom7 = new Uom();
        uom7.setDescription("Pint");
        uomRepository.save(uom7).block();

        Uom uom8 = new Uom();
        uom8.setDescription("Dash");
        uomRepository.save(uom8).block();
    }
}
