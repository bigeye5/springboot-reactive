package za.co.limehouse.tuts.springbootreactive.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import za.co.limehouse.tuts.springbootreactive.Constants;
import za.co.limehouse.tuts.springbootreactive.domain.Uom;
import za.co.limehouse.tuts.springbootreactive.model.UomDto;
import za.co.limehouse.tuts.springbootreactive.service.UomService;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/" + Constants.VERSION + "/uom")
@Slf4j
public class UomController {
    private UomService uomService;

    @Autowired
    public UomController(UomService uomService) {
        this.uomService = uomService;
    }

    @GetMapping("/")
    public Flux<UomDto> findAll() {
        log.debug("findAll was called");
        return uomService.findAll().map(u -> new UomDto(u))
                .switchIfEmpty(Flux.error(
                        new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.HTTP_RESOURCE_NOT_FOUND)
                ));
    }

    @GetMapping("/{id}")
    public Mono<UomDto> findById(@PathVariable String id) {
        log.debug("Finding UOM: {}", id);
        return uomService.findById(id).map(u -> new UomDto(u))
                .switchIfEmpty(Mono.error(
                        new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.HTTP_RESOURCE_NOT_FOUND)
                ));
    }

    @PostMapping("/search")
    public Flux<UomDto> search(@RequestBody Map<String, String> searchFilter) {
        if (isValidSearch(searchFilter)) {
            return uomService.search(searchFilter).map(u -> new UomDto(u))
                    .switchIfEmpty(Flux.error(
                            new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.HTTP_RESOURCE_NOT_FOUND)
                    ));
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constants.HTTP_BAD_REQUEST);
        }
    }

    @PostMapping("/")
    public Mono<UomDto> insert(@RequestBody Map<String, String> body) {
        Uom uom = new Uom();
        uom.setDescription(body.get("description"));
        if (isValidInsert(uom)) {
            return uomService.insert(uom).map(u -> new UomDto(u));
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constants.HTTP_BAD_REQUEST);
        }
    }

    @PutMapping("/")
    public Mono<UomDto> update(@RequestBody Map<String, String> body) {

        String id = body.get("id");
        String description = body.get("description");
        if (isValidUpdate(id, description)) {
            return uomService
                    .findById(id)
                    .map(u -> new Uom(id, description))
                    .flatMap(uomService::update)
                    .map(u -> new UomDto(u))
                    .switchIfEmpty(Mono.error(
                            new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.HTTP_RESOURCE_NOT_FOUND)
                    ));
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constants.HTTP_BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    public Mono<Uom> delete(@PathVariable String id) {
        return uomService
                .findById(id)
                .flatMap(u -> uomService.deleteById(u.getId()).thenReturn(u))
                .switchIfEmpty(Mono.error(
                        new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.HTTP_RESOURCE_NOT_FOUND)
                ));
    }

    private boolean isValidInsert(Uom uom) {
        return uom.getDescription().isBlank() ? false : true;
    }

    private boolean isValidUpdate(String id, String description) {

        if (id.isBlank() || id.length() != Constants.UUID_LENGTH || description.isBlank()) {
            return false;
        }
        return true;
    }

    private boolean isValidSearch(Map<String, String> searchFilter) {
        return searchFilter.containsKey("description");
    }
}
