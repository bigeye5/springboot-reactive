package za.co.limehouse.tuts.springbootreactive.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import za.co.limehouse.tuts.springbootreactive.domain.Uom;

import java.util.Map;

public interface UomService {

    Flux<Uom> findAll();
    Mono<Uom> insert(Uom uom);
    Mono<Uom> update(Uom uom);
    Mono<Void> deleteById(String id);
    Mono<Uom> findById(String id);
    Flux<Uom> search(Map<String, String> searchFilter);
}
