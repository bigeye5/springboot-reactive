package za.co.limehouse.tuts.springbootreactive.model;

import lombok.Getter;
import lombok.Setter;
import za.co.limehouse.tuts.springbootreactive.domain.Uom;

@Getter
@Setter
public class UomDto {

    private String id;
    private String desc;

    public UomDto(Uom uom) {
        this.id = uom.getId();
        this.desc = uom.getDescription();
    }
}
