package za.co.limehouse.tuts.springbootreactive.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "uom")
public class Uom {

    private String id;
    private String description;

    public Uom() {

    }

    public Uom(String id, String description) {
        this.id = id;
        this.description = description;
    };
}
