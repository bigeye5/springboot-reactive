package za.co.limehouse.tuts.springbootreactive;

public class Constants {
    public static final String VERSION = "v1";
    public static final String HTTP_RESOURCE_NOT_FOUND = "Resource not found";
    public static final String HTTP_BAD_REQUEST = "Invalid request. Ensure all required data fields are populated correctly";
    public static final int UUID_LENGTH = 24;
}
