package za.co.limehouse.tuts.springbootreactive.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import za.co.limehouse.tuts.springbootreactive.domain.Uom;


public interface UomRepository extends ReactiveMongoRepository<Uom, String> {

    Flux<Uom> findByDescriptionContainingIgnoreCase(String description);

}
